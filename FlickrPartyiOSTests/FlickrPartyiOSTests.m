//
//  FlickrPartyiOSTests.m
//  FlickrPartyiOSTests
//
//  Created by Pawel Maczewski on 15/03/16.
//  Copyright © 2016 owlcoding. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "MockURLSession.h"
#import "NetworkRequest.h"
#import "PhotoModel.h"

@interface FlickrPartyiOSTests : XCTestCase
@property (nonatomic, strong) NetworkRequest *request;
@end

@implementation FlickrPartyiOSTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.request = [NetworkRequest shared];
    MockURLSession *session = [MockURLSession new];
    _request.session = session;
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void) testIncorrectJSONResponse {
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    MockURLSession *session = _request.session;
    session.errorTextFromFlickr = @"Can't find any photo";

    // based on the http://blog.dadabeatnik.com/2013/09/12/xcode-and-asynchronous-unit-testing/
    // Set the flag to YES
    __block BOOL waitingForBlock = YES;

    [_request fetchListOfPhotosForPage:1
                   withCompletion:^(NSArray * _Nullable photos, NSInteger pageReturned, NSError * _Nullable error) {
                       waitingForBlock = NO;
                       XCTAssertNil(photos);
                       XCTAssertNotEqual(session.errorTextFromFlickr, error.userInfo[NSLocalizedDescriptionKey]);
                       XCTAssertEqual(pageReturned, 1);
                   }];
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
}
- (void) testCorrectJSONResponse {
    __block BOOL waitingForBlock = YES;
    
    [_request fetchListOfPhotosForPage:1
                        withCompletion:^(NSArray * _Nullable photos, NSInteger pageReturned, NSError * _Nullable error) {
                            waitingForBlock = NO;
                            XCTAssertNil(error);
                            XCTAssertEqual(photos.count, 1);
                            XCTAssertEqual(pageReturned, 1);
                            PhotoModel *photo = photos [ 0 ];
                            
                            PhotoModel *fromDict = [[PhotoModel alloc] initWithDictionary:((MockURLSession *)_request.session).photoDict];
                            XCTAssertEqualObjects(photo, fromDict);
                        }];
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
}


@end
