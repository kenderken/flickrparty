//
//  MockURLSession.h
//  FlickrPartyiOS
//
//  Created by Pawel Maczewski on 15/03/16.
//  Copyright © 2016 owlcoding. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkRequest.h"

@interface MockURLSession : NSObject <URLSessionProtocol>


@property (nonatomic, copy) NSString *errorTextFromFlickr;
@property (nonatomic, strong) NSDictionary * photoDict;
@end

