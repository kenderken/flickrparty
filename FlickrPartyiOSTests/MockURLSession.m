//
//  MockURLSession.m
//  FlickrPartyiOS
//
//  Created by Pawel Maczewski on 15/03/16.
//  Copyright © 2016 owlcoding. All rights reserved.
//

#import "MockURLSession.h"


@implementation MockURLSession


- (id) init
{
    self = [super init];
    self.photoDict = @{
                  @"id": @"ID",
                  @"owner": @"OWNER",
                  @"secret": @"SECRET",
                  @"server": @"SERVER",
                  @"farm": @1,
                  @"title": @"TITLE",
                  @"ispublic": @1,
                  @"isfriend": @0,
                  @"isfamily": @0
                  };
    return self;
}
- (NSURLSessionDataTask *) dataTaskWithURL:(NSURL *)url completionHandler:(void (^)(NSData * _Nullable, NSURLResponse * _Nullable, NSError * _Nullable))completionHandler
{
    NSDictionary *json;

    if (_errorTextFromFlickr) {
        json = @{
                 @"stat": @"fail",
                 @"code": @3,
                 @"message": _errorTextFromFlickr
                 };
    } else {
        json = @{
                 @"photos": @{
                         @"page": @1,
                         @"pages": @"2221",
                         @"perpage": @1,
                         @"total": @"222021",
                         @"photo": @[
                                 self.photoDict,
                                 ]
                         },
                 @"stat": @"ok"
                 };
    }
    NSError *err = nil;
    NSData *data = [NSJSONSerialization dataWithJSONObject:json
                                                   options:NSJSONWritingPrettyPrinted
                                                     error:&err];
    completionHandler ( data, nil, nil );
    return nil;
}
@end
