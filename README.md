# Flickr Party iOS test project
The task was to create a Flickr photos browser for a tag `Party`. 
## Notes
A few notes about the implementation:

* No XIB is used in the project. As requested.
* No Storyboard is used for the UI. As requested. There is a `Launch Screen.storyboard` file that nicely handles the larger iPhones screen usage. 
* The recommendation was to use **as few** external libraries as possible. I didn't use any. For a larger project, interacting heavily with an API I would probably use `AFNetworking` (or something similar). For a project as simple as this one I didn't see the reason to include whole `AFNetworking`, as I can simply make a `NSURLSession` request to fetch the data from Flickr. 
* Same comes to displaying of remote photos. `AFNetworking` has a nice `UIKit` categories that include the `UIImageView` category to handle that task. But, not using that library for network communication, I didn't want to include it just to display the images. So, my `RemoteImageView` class does that.
* The photos browser allows to zoom in/out the photos on the details view and allows for switching the photos to previous and next using the buttons on the navigation bar. 
* Total work time: 3 hours 48 minutes (12 minutes spent on Flickr website). 