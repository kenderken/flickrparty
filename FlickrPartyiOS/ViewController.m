//
//  ViewController.m
//  FlickrPartyiOS
//
//  Created by Pawel Maczewski on 10/03/16.
//  Copyright © 2016 owlcoding. All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>

#import "ViewController.h"
#import "PhotosDetailsViewController.h"

#import "PhotoCollectionViewCell.h"
#import "NetworkRequest.h"
#import "PhotoModel.h"

static NSInteger cellSize = 75;

@interface ViewController ()
@property (nonatomic, weak) UICollectionView *photosCollection;
@property (nonatomic, strong) UICollectionViewFlowLayout *collectionLayout;

@property (nonatomic, strong) NSArray *photosList;

@property (atomic, assign) BOOL isUpdatingPhotos;
@property (nonatomic, assign) NSInteger currentFlicrResultsPage;

@end

@implementation ViewController

- (void) loadView
{
    // create collection view
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    
    UICollectionView *collection = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    self.collectionLayout = layout;
    self.photosCollection = collection;
    self.view = collection;
}
- (void) updatePhotosForNextPage
{
    if (self.isUpdatingPhotos) {
        return;
    }
    self.isUpdatingPhotos = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES].labelText = @"Updating photos list...";
    NSLog(@"Fetching photos for page: %d", self.currentFlicrResultsPage + 1);
    [[NetworkRequest shared] fetchListOfPhotosForPage:(self.currentFlicrResultsPage + 1) withCompletion:^(NSArray *photos, NSInteger currentPage, NSError *error) {
        self.isUpdatingPhotos = NO;
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if (!error) {
            self.currentFlicrResultsPage = currentPage;
            self.photosList = [self.photosList arrayByAddingObjectsFromArray:photos];
            [self.photosCollection reloadData];
        } else {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                           message:error.localizedDescription
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Retry"
                                                      style:UIAlertActionStyleCancel
                                                    handler:^(UIAlertAction * _Nonnull action) {
                                                        [self updatePhotosForNextPage];
                                                    }]];
            [self presentViewController:alert
                               animated:YES
                             completion:nil];
        }
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];

    _photosCollection.backgroundColor = [UIColor whiteColor];
    
    _photosCollection.dataSource = self;
    _photosCollection.delegate = self;
    
    [_photosCollection registerClass:[PhotoCollectionViewCell class] forCellWithReuseIdentifier:@"PhotoCell"];
    

    self.title = @"Flickr Party";
    self.isUpdatingPhotos = NO;
    self.currentFlicrResultsPage = 0;
    self.photosList = @[];
    [self updatePhotosForNextPage];
}

#pragma mark - Collection View Data Source
- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _photosList.count;
}
- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger idx = indexPath.row;
    PhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PhotoCell" forIndexPath:indexPath];
    cell.indexPath = indexPath;
    PhotoModel *photo = _photosList [ idx ];
    [cell setPhoto:photo];
    cell.backgroundColor = [UIColor whiteColor];
    return cell;
}

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(cellSize, cellSize);
}
- (UIEdgeInsets) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(5, 5, 5, 5);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}

#pragma mark - Collection View Delegate
// This handles auto-fetching more results as we scroll to the bottom...
- (void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat distanceToBottom = scrollView.contentSize.height - scrollView.contentOffset.y - scrollView.bounds.size.height;
    if (distanceToBottom < 10 * cellSize) {
        [self updatePhotosForNextPage];
    }
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PhotosDetailsViewController *detailsVC = [[PhotosDetailsViewController alloc] init];
    detailsVC.photosArray = self.photosList;
    detailsVC.currentPhotoIndex = indexPath.row;
    [self.navigationController pushViewController:detailsVC
                                         animated:YES];
}

@end
