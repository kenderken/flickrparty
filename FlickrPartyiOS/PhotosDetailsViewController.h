//
//  PhotosDetailsViewController.h
//  FlickrPartyiOS
//
//  Created by Pawel Maczewski on 10/03/16.
//  Copyright © 2016 owlcoding. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoModel.h"

@interface PhotosDetailsViewController : UIViewController <UIScrollViewDelegate>

@property (nonatomic, weak) NSArray *photosArray;
@property (nonatomic, assign) NSInteger currentPhotoIndex;

@end
