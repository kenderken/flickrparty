//
//  main.m
//  FlickrPartyiOS
//
//  Created by Pawel Maczewski on 10/03/16.
//  Copyright © 2016 owlcoding. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
