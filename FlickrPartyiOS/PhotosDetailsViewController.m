//
//  PhotosDetailsViewController.m
//  FlickrPartyiOS
//
//  Created by Pawel Maczewski on 10/03/16.
//  Copyright © 2016 owlcoding. All rights reserved.
//

#import "PhotosDetailsViewController.h"

#import "RemoteImageView.h"

@interface PhotosDetailsViewController ()
@property (nonatomic, weak) UIScrollView *scrollviewForPhoto;
@property (nonatomic, weak) RemoteImageView *imageView;
@property (atomic, assign) BOOL switchingPhotos;
@end

@implementation PhotosDetailsViewController
- (void) loadView
{
    UIScrollView *scrollviewForPhoto = [[UIScrollView alloc] initWithFrame:CGRectZero];
    scrollviewForPhoto.backgroundColor = [UIColor whiteColor];
    scrollviewForPhoto.alwaysBounceHorizontal = YES;
    self.scrollviewForPhoto = scrollviewForPhoto;
    self.view = scrollviewForPhoto;
}

- (void) nextPhoto:(UIBarButtonItem *) sender
{
    self.currentPhotoIndex = MIN(self.currentPhotoIndex + 1, self.photosArray.count - 1);
    [self displayPhotoAtIndex:self.currentPhotoIndex];
}
- (void) prevPhoto:(UIBarButtonItem *) sender
{
    self.currentPhotoIndex = MAX(self.currentPhotoIndex - 1, 0);
    [self displayPhotoAtIndex:self.currentPhotoIndex];
}
- (void) displayPhotoAtIndex:(NSInteger) idx
{
    self.switchingPhotos = YES;
    if (idx < 0) {
        idx = 0;
    }
    if (idx >= self.photosArray.count) {
        idx = self.photosArray.count - 1;
    }
    PhotoModel *photo = self.photosArray [ idx ];
    __weak typeof(self.imageView) _weak = self.imageView; // to access the imageView inside the block
    self.scrollviewForPhoto.minimumZoomScale = 1;
    self.scrollviewForPhoto.maximumZoomScale = 1;
    self.scrollviewForPhoto.zoomScale = 1;
    [self.imageView setImageWithURL:[photo viewURL]
                   placeholderImage:[UIImage imageNamed:@"default-placeholder"]
                     withCompletion:^(UIImage *image, NSError *error) {
                         _weak.frame = CGRectMake(0, 0, image.size.width, image.size.height);
                         _weak.image = image;
                         self.scrollviewForPhoto.contentSize = image.size;

                         // calculate scale for the image so it fits the screen
                         CGFloat yScale = self.scrollviewForPhoto.bounds.size.height / image.size.height;
                         CGFloat xScale = self.scrollviewForPhoto.bounds.size.width / image.size.width;
                         CGFloat scale = MIN(xScale, yScale);
                         self.scrollviewForPhoto.minimumZoomScale = scale;
                         self.scrollviewForPhoto.maximumZoomScale = 1;
                         self.scrollviewForPhoto.zoomScale = scale;
                         self.switchingPhotos = NO;
                         [self scrollViewDidZoom:self.scrollviewForPhoto];
                     }];
    self.title = photo.title;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // next and prev buttons
    UIBarButtonItem *prev = [[UIBarButtonItem alloc] initWithTitle:@"<"
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:@selector(prevPhoto:)];
    UIBarButtonItem *next = [[UIBarButtonItem alloc] initWithTitle:@">"
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:@selector(nextPhoto:)];
    self.navigationItem.rightBarButtonItems = @[next, prev];

    
    RemoteImageView *imageView = [[RemoteImageView alloc] init];
    [self.scrollviewForPhoto addSubview:imageView];
    self.imageView = imageView;
    self.scrollviewForPhoto.delegate = self;
    
    [self displayPhotoAtIndex:self.currentPhotoIndex];
}

- (UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}
- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    // center after zooming
    CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?
    (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
    CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)?
    (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0;
    self.imageView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                   scrollView.contentSize.height * 0.5 + offsetY);
}
@end
