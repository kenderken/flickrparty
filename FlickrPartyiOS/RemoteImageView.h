//
//  RemoteImageView.h
//  FlickrPartyiOS
//
//  Created by Pawel Maczewski on 10/03/16.
//  Copyright © 2016 owlcoding. All rights reserved.
//
//  This class handles displaying of remote image.
//  Instead of using the category from AFNetworking, I used this simple class
//  Saving some large chunk of MB from the final binary :)
//

#import <UIKit/UIKit.h>
typedef void(^ImageCompletionBlock)(UIImage *image, NSError *error);
@interface RemoteImageView : UIImageView

@property (nonatomic, weak) NSURLSessionDataTask *downloadTask;
- (NSURLSessionDataTask *) setImageWithURL:(NSURL *) url placeholderImage:(UIImage *) placeholder;
- (NSURLSessionDataTask *) setImageWithURL:(NSURL *) url placeholderImage:(UIImage *) placeholder withCompletion:(ImageCompletionBlock) completion;

- (void) cancelImageDownloadOperation;

@end

