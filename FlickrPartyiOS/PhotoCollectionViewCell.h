//
//  PhotoCollectionViewCell.h
//  FlickrPartyiOS
//
//  Created by Pawel Maczewski on 10/03/16.
//  Copyright © 2016 owlcoding. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoModel.h"

@interface PhotoCollectionViewCell : UICollectionViewCell

@property (nonatomic, copy) NSURL *url;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSIndexPath *indexPath;
- (void) setPhoto:(PhotoModel *) photo;
@end
