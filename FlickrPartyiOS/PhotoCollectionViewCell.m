//
//  PhotoCollectionViewCell.m
//  FlickrPartyiOS
//
//  Created by Pawel Maczewski on 10/03/16.
//  Copyright © 2016 owlcoding. All rights reserved.
//

#import "PhotoCollectionViewCell.h"
#import "RemoteImageView.h"

@interface PhotoCollectionViewCell ()
@property (nonatomic, strong) RemoteImageView *thumbnailView;
@end
@implementation PhotoCollectionViewCell

- (void) prepareForReuse
{
    [self.thumbnailView cancelImageDownloadOperation];
    [self setPhoto:nil];
}
- (void) display
{
    __weak typeof(self) weakself = self;
    __block NSIndexPath *indexPath = self.indexPath;
    if (self.url) {
        [self.thumbnailView setImageWithURL:self.url placeholderImage:[UIImage imageNamed:@"default-placeholder"] withCompletion:^(UIImage *image, NSError *error) {
            if ([weakself.indexPath isEqual:indexPath]) {
                weakself.thumbnailView.image = image;
            }
        }];
    } else {
        self.thumbnailView.image = [UIImage imageNamed:@"default-placeholder"];
    }
}
- (instancetype) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];

    RemoteImageView *imageView = [[RemoteImageView alloc] initWithFrame:self.bounds];
    [self addSubview:imageView];
    self.thumbnailView = imageView;
    
    return self;
}

- (void) setPhoto:(PhotoModel *)photo
{
    self.title = photo.title;
    self.url = [photo thumbnailURL];
    [self display];
}
@end
