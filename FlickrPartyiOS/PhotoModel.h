//
//  PhotoModel.h
//  FlickrPartyiOS
//
//  Created by Pawel Maczewski on 10/03/16.
//  Copyright © 2016 owlcoding. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 Flickr gives me the following JSON:
 { "id": "25355523890", "owner": "93459915@N07", "secret": "ac166ceda4", "server": "1495", "farm": 2, "title": "2016OakBrookPlunge-ST0727", "ispublic": 1, "isfriend": 0, "isfamily": 0 },
 
 */

@interface PhotoModel : NSObject 

@property (nonatomic, copy) NSString *title;
@property (nonatomic, assign) NSInteger farm;
@property (nonatomic, copy) NSString *server;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *secret;

/*!
 https://farm{{farm}}.staticflickr.com/{{server}}/{{id}}_{{secret}}_{{size}}.jpg
 where size is:
 
 s	small square 75x75
 
 t	thumbnail, 100 on longest side
 
 m	small, 240 on longest side
 
 z	medium 640, 640 on longest side
 
 b	large, 1024 on longest side
 */

- (NSURL *) thumbnailURL;
- (NSURL *) viewURL;


/*!
 * @discussion Designed initializer
 * @param Dictionary, as returned by JSON Flickr rest api call
 * @return PhotoModel
 */

- (instancetype) initWithDictionary:(NSDictionary *) dictionary;
@end
