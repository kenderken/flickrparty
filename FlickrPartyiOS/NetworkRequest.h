//
//  NetworkRequest.h
//  FlickrPartyiOS
//
//  Created by Pawel Maczewski on 10/03/16.
//  Copyright © 2016 owlcoding. All rights reserved.
//
//  This class handles network requests.
//  Instead of using the AFNetworking, I used this simple class.
//  For a single request it's enough for a small project like this.
//  This saves some large chunk of MB from the final binary :)
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol URLSessionProtocol <NSObject>

- (NSURLSessionDataTask * _Nullable)dataTaskWithURL:(NSURL * _Nonnull)url completionHandler:( void (^ _Nullable )(NSData * __nullable data, NSURLResponse * __nullable response, NSError * __nullable error))completionHandler;

@end

typedef void(^PhotosCompletion)(NSArray * _Nullable photos, NSInteger pageReturned, NSError * _Nullable error);




@interface NetworkRequest : NSObject

@property (nonatomic, strong) _Nonnull id<URLSessionProtocol> session;

+ (instancetype _Nonnull) shared;
- (void) fetchListOfPhotosForPage:(NSInteger ) page withCompletion:(PhotosCompletion _Nullable)completion;

@end

@interface NSURLSession (ProtocolResponds) <URLSessionProtocol>

@end