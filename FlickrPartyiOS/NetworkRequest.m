//
//  NetworkRequest.m
//  FlickrPartyiOS
//
//  Created by Pawel Maczewski on 10/03/16.
//  Copyright © 2016 owlcoding. All rights reserved.
//

#import "NetworkRequest.h"
#import "PhotoModel.h"

@implementation NetworkRequest

static NetworkRequest * _shared = nil;
+ (instancetype) shared
{
    if (_shared == nil) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            _shared = [[NetworkRequest alloc] init];
            _shared.session = [NSURLSession sharedSession];
        });
    }
    return _shared;
}
- (void) fetchListOfPhotosForPage:(NSInteger ) page withCompletion:(PhotosCompletion)completion
{
    
    NSString *sURL = [NSString stringWithFormat:@"https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=7978778a901b2d546eb5a23610e67a96&tags=Party&per_page=100&page=%ld&format=json&nojsoncallback=1", (long)page];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [[_session dataTaskWithURL:[NSURL URLWithString:sURL]
            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                NSLog(@"error: %@", error);
                if (!error) {
                    // do the json parsing and PhotoModel objects creating in the background... (we're in background thread here)
                    NSError *err = nil;
                    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                                         options:NSJSONReadingAllowFragments
                                                                           error:&err];
                    
                    if (err == nil) {
                        // check if there's no Flickr-side error...
                        if ([[json objectForKey:@"stat"] isEqualToString:@"fail"]) {
                            NSError *error = [NSError errorWithDomain:@"Flickr Error"
                                                                 code:[[json objectForKey:@"code"] integerValue]
                                                             userInfo:@{NSLocalizedDescriptionKey : [json objectForKey:@"message"]}];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                completion ( nil, page, error );
                            });
                            return ;
                        }

                        // current page
                        NSDictionary *photos = [json objectForKey:@"photos"];
                        NSInteger currentPage = [[photos objectForKey:@"page"] integerValue];
                        NSInteger totalPages = [[photos objectForKey:@"pages"] integerValue];
                        // don't return photos for pages that don't exist (no idea what Flickr returns here, seems like 1st page...)
                        if (currentPage > totalPages) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                completion ( @[], totalPages, nil );
                            });
                        } else {
                            
                            NSMutableArray *arr = [NSMutableArray arrayWithCapacity:[[photos objectForKey:@"photo"] count]];
                            
                            for (NSDictionary *photoDict in photos[@"photo"]) {
                                PhotoModel *photo = [[PhotoModel alloc] initWithDictionary:photoDict];
                                [arr addObject:photo];
                            }
                            dispatch_async(dispatch_get_main_queue(), ^{
                                completion ( [NSArray arrayWithArray:arr], currentPage, nil );
                            });
                        }
                    } else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            completion ( nil, 0, error );
                        });
                    }

                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion ( nil, 0, error );
                    });
                }
            }] resume];
}

@end

