//
//  PhotoModel.m
//  FlickrPartyiOS
//
//  Created by Pawel Maczewski on 10/03/16.
//  Copyright © 2016 owlcoding. All rights reserved.
//

#import "PhotoModel.h"

@implementation PhotoModel

- (NSString *) baseURLWithSizeParam:(NSString *) sizeParam
{
    return [NSString stringWithFormat:
            @"https://farm%ld.staticflickr.com/%@/%@_%@_%@.jpg",
            (long)self.farm,
            self.server,
            self.ID,
            self.secret,
            sizeParam
            ];

}
- (NSURL *) thumbnailURL
{
    return [NSURL URLWithString:[self baseURLWithSizeParam:@"s"]];
}
- (NSURL *) viewURL
{
    return [NSURL URLWithString:[self baseURLWithSizeParam:@"b"]];
}

- (instancetype) initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    self.farm = [[dictionary objectForKey:@"farm"] integerValue];
    self.server = [dictionary objectForKey:@"server"];
    self.ID = [dictionary objectForKey:@"id"];
    self.secret = [dictionary objectForKey:@"secret"];
    self.title = [dictionary objectForKey:@"title"];
    return self;
}

- (BOOL) isEqual:(id)object
{
    if ([object class] != [self class]) {
        return NO;
    } else {
        if ([[self viewURL] isEqual:[object viewURL]]) {
            return YES;
        } else {
            return NO;
        }
    }
}

@end
