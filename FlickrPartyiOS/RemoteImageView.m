//
//  RemoteImageView.m
//  FlickrPartyiOS
//
//  Created by Pawel Maczewski on 10/03/16.
//  Copyright © 2016 owlcoding. All rights reserved.
//

#import "RemoteImageView.h"

@implementation RemoteImageView

- (NSURLSessionDataTask *) setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder
{
    __weak typeof(self) _weakself = self;
    return [self setImageWithURL:url
                placeholderImage:placeholder
                  withCompletion:^(UIImage *image, NSError *error) {
                      if (!error) {
                          _weakself.image = image;
                      }
                  }];
}
- (NSURLSessionDataTask *) setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder withCompletion:(ImageCompletionBlock)completion
{
    self.image = placeholder;
    if (self.downloadTask) {
        [self cancelImageDownloadOperation];
    }
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url
                                                             completionHandler:^(NSData *data, NSURLResponse *response, NSError* error) {
                                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                                     // in main queue
                                                                     if (!error) {
                                                                         completion ( [UIImage imageWithData:data], nil );
                                                                     } else {
                                                                         completion ( nil, error );
                                                                     }
                                                                     self.downloadTask = nil;
                                                                 });
                                                             }];
    [task resume];
    self.downloadTask = task;
    return task;
}
- (void) cancelImageDownloadOperation
{
    [self.downloadTask cancel];
    self.image = nil;
    self.downloadTask = nil;
}
@end
